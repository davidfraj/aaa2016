<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>web01</title>
	<link rel="stylesheet" href="estilos.css">
</head>
<body>
	
	<!-- section>header+(section>(nav+div))+footer -->
	<section>
		<header><?php require_once("inc/encabezado.php"); ?></header>
		<section>
			<nav><?php require_once("inc/menu.php"); ?></nav>
			<div>
				
			<h2>Contacto</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi adipisci blanditiis est reiciendis necessitatibus, ea dolorem porro ut minima consectetur alias. Deleniti sit totam, sunt. Unde soluta necessitatibus esse, porro!</p>
				<p>Nesciunt beatae omnis, odio officia, voluptatibus magnam eius delectus voluptate enim deserunt molestiae totam quas! Ab, possimus id a culpa et facere. Voluptates suscipit sunt recusandae impedit nam dolorum, autem!</p>
				<p>Ipsum quos facere quisquam beatae, odit reprehenderit sit. Incidunt quos, veniam aliquid, consectetur rem suscipit deserunt tempora temporibus repellat repudiandae vel. Quod doloribus unde labore necessitatibus inventore harum fugiat quaerat?</p>
				<p>Molestiae animi tempore odit excepturi! Corrupti adipisci earum, quos harum amet, mollitia est, eaque beatae, consequatur quia quasi! Praesentium sit cumque error facilis perferendis, a pariatur odio adipisci, atque voluptatum.</p>

			</div>
		</section>
		<footer><?php require_once("inc/pie.php"); ?></footer>
	</section>

</body>
</html>