<?php 

$meses=array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

function saluda(){
	$r="Buenas tardes";
	return $r;	
}

function suma($a, $b){
	$r=$a+$b;
	return $r;
}

function dimeMes($numero=1){
	global $meses;
	return $meses[$numero];
}

function encabezado(){
	echo '<head>';
	echo '<title>Titulo en la web</title>';
	echo '</head>';
	global $meses;
	$meses[0]="Otra cosa";
}

function copyright($inicio){
	$actual=date('Y');
	$r='';
	if($actual==$inicio){
		$r='Copyright '.$actual;
	}elseif($actual>$inicio){
		$r='Copyright '.$inicio.' - '.$actual;
	}else{
		$r="ERROR DE AÑO";
	}
	return $r;
}

//Paso de variables por referencia
function agregar(&$numero, $valor){
	$numero+=$valor;
}

function agregar2($numero, $valor){
	$numero+=$valor;
	return $numero;
}

$cantidad=100;
agregar($cantidad, 20);
echo $cantidad;

$cantidad=agregar2($cantidad, 30);

function imagen($url, $ancho){
	$r='<a href="'.$url.'">';
	$r.='<img src="'.$url.'" width="'.$ancho.'">';
	$r.='</a>';
	return $r;
}


function desplegable($vector){
	if(is_array($vector)){
		$r='<select>';
		for ($i=0; $i < count($vector); $i++) { 
			$r.='<option>'.$vector[$i].'</option>';
		}
		$r.='</select>';
	}else{
		$r='Necesito un vector';
	}
	return $r;
}
echo desplegable($meses);


$paginas=array('inicio.php', 'contacto.php', 'noticias.php', 'galeria.php');
$titulos=array('Home', 'Contactar', 'Noticias de la web', 'Galeria de imagenes');

echo menu($paginas, $titulos, 'galeria.php');

function menu($paginas, $titulos, $actual){
	$r='<ul>';
	for ($i=0; $i < count($paginas); $i++) { 
		if($paginas[$i]==$actual){
			$r.='<li style="background-color: red;">';
		}else{
			$r.='<li>';
		}
		$r.='<a href="index.php?pagina='.$paginas[$i].'">';
		$r.=$titulos[$i];
		$r.='</a>';
		$r.='</li>';
	}
	$r.='</ul>';
	return $r;
}


echo hola();

function hola(){
	return "Hola";
}


//$miMenu=array('noticias.php'=>'Noticias de la web')

//$miMenu['noticias.php']='Noticias de la web';

//Copyright 2012 - 2016
//Copyright 2016
// date('Y');
?>

<!-- ul>li*4>a[href="index.php?pagina=noticias.php"]{Titulo de enlace} -->

