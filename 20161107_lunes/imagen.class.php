<?php 

//Creamos una clase llamada imagen
//Fecha: 
//historico: 
//con final class, NO permitimos herencia

class Imagen{
	//public, private, protected
	public $archivo;
	public $titulo;
	public $borde=2;
	protected $ancho;

	public function __construct($archivo, $titulo){
		$this->archivo=$archivo;
		$this->titulo=$titulo;
		//$this->borde=0;
		$this->ancho=400;
	}

	public function dibujar(){
		return '<img src="'.$this->archivo.'" title="'.$this->titulo.'" border="'.$this->borde.'" width="'.$this->ancho.'"><br>';
	}

	public function setAncho($ancho){
		if(is_numeric($ancho)){
			$this->ancho=$ancho;
		}else{
			echo "Tienes que introducir un numero";
		}
	}

	public function dimeTitulo(){
		return '<caption>'.$this->titulo.'</caption>';
	}
}	

?>