<?php 
$titulo="Hola mundo 2";
$tipoLetra="verdana";
$idioma="en";
$ancho=500;
 ?>

<!DOCTYPE html>
<html lang="<?php echo $idioma;?>">
<head>
	<meta charset="UTF-8">
	<title><?php echo $titulo; ?></title>
	<style type="text/css">
		h1{
			font-family: <?php echo $tipoLetra; ?>
		}
	</style>
	<script type="text/javascript">
		alert("<?php echo $titulo; ?>");
	</script>
	<link rel="stylesheet" href="css/estilos_<?php echo $idioma;?>.css">
</head>
<body>
	<h1><?php echo $titulo; ?></h1>
	<table width="<?php $ancho;?>"></table>
</body>
</html>