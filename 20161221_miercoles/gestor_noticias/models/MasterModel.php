<?php
Class MasterModel{
	protected $conexion;
	protected $elementos;
	protected $elementosPorPagina;
	protected $orden;
	protected $campoOrden;
	protected $tabla;

	public function __construct($tabla){
		$this->conexion=Conexion::conectar();
		$this->elementos=array();
		$this->elementosPorPagina=NEPP;
		$this->orden='ASC';
		$this->tabla=$tabla;
		
		$sql="SELECT * FROM $tabla LIMIT 0,1";
		$consulta=$this->conexion->query($sql);
		$this->campoOrden=$consulta->fetch_fields()[0]->name;

	}
	
	public function elementosPorPagina($numElementos=NEPP){
		if($numElementos>0){
			$this->elementosPorPagina=$numElementos;
		}
	}

	public function dimePaginas(){
		$sql="SELECT * FROM $this->tabla";
		$consulta=$this->conexion->query($sql);
		$total=$consulta->num_rows;
		return ceil($total/$this->elementosPorPagina);
	}

	public function estableceOrden($campo, $orden){
		$this->orden=$orden;
		$this->campoOrden=$campo;
	}

	public function listado($numpag){
		$n=($numpag-1)*$this->elementosPorPagina;
		$sql="SELECT * FROM $this->tabla ORDER BY $this->campoOrden $this->orden LIMIT $n,$this->elementosPorPagina";
		$consulta=$this->conexion->query($sql);
		while($fila=$consulta->fetch_array()){
			$this->elementos[]=$fila;
		}
		return $this->elementos;
	}

}
?>