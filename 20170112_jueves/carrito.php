<h3>Cesta de compra</h3>
<?php  
//Voy a tener una VARIABLE de session para la cesta
//Lo primero SIEMPRE es comprobar SI existe esa variable
if(!isset($_SESSION['cesta'])){
	$_SESSION['cesta']=array();
}

//Agregar productos
if(isset($_GET['idCompra'])){
	$_SESSION['cesta'][]=$_GET['idCompra'];
}

//Quitar productos
if(isset($_GET['quitar'])){
	array_splice($_SESSION['cesta'], $_GET['quitar'], 1);
}

//Vaciar el carrito
if(isset($_GET['vaciarcesta'])){
	$_SESSION['cesta']=array();
}

//Mostrar el carrito
$total=0;
for ($i=0; $i < count($_SESSION['cesta']); $i++) { 

	$sql="SELECT * FROM productos WHERE idPro=".$_SESSION['cesta'][$i];
	$consulta=$conexion->query($sql);
	$fila=$consulta->fetch_array();
	echo $fila['nombrePro'].'('.$fila['precioPro'].' €)';
	echo '<a href="index.php?quitar='.$i.'">Q</a>';
	echo '<br>';
	$total+=$fila['precioPro'];

}

?>
<p><?php echo count($_SESSION['cesta']); ?> productos</p>
<p>Total: <?php echo $total; ?> Euros</p>
<a href="index.php?vaciarcesta=si">Vaciar</a>