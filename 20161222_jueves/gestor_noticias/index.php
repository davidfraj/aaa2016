<?php  
require_once('config.php');
require_once('funciones.php');
require_once('includes/conexion.php');
require_once('models/MasterModel.php');
?>
<!-- html:5>section>(header+nav+section+footer) -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Gestor de Noticias</title>
	<link rel="stylesheet" href="css/estilos.css">
</head>
<body>
	<section>
		<header>
			ENCABEZADO
			<?php //cargarModulo('banner');?>
		</header>
		<nav>
			<?php cargarModulo('menu'); ?>
		</nav>
		<section>
			<!-- CONTENIDO DEL CONTROLADOR -->
			<?php  
			if(isset($_GET['p'])){
				$p=$_GET['p'];
			}else{
				$p='noticias.php';
			}
			include('controllers/'.$p);
			?>
			<!-- FIN CONTENIDO DEL CONTROLADOR -->
		</section>
		<footer>PIE DE PAGINA</footer>
	</section>
</body>
</html>