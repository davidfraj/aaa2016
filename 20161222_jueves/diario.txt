Jueves 22 de Diciembre de 2016
-----------------------------

Creamos un modulo que muestre un banner publicitario, que al hacer click, nos lleve a un enlace de publicidad de una web.

El banner publicitario debe ser una imagen debajo del encabezado, que llamaremos con la funcion:

cargarModulo('banner');

Debe mostrar un banner aleatorio.

Lo cargaremos en el controlador ('banner.php') de esta manera:

$imagenes=array('imagen1.jpg', 'imagen2.jpg', 'imagen3.jpg');
$enlaces=array('http://www.amazon.es.....', 'http://www.ebay.com', 'http://...');
$banner=new bannerModel($imagenes, $enlaces);

---------------------------------------

Proyecto de administracion de backend con base de datos:

Queremos crear un panel de control, con caracteristicas CRUD sobre base de datos, para administrar un hotel.

Debemos poder tener las siguientes caracteristicas:

	El hotel tiene:
		Habitaciones
		Tipos de habitaciones
		Plantas en la habitacion
		Clientes

	Queremos poder realizar las siguientes acciones:
		Administrar plantas (CRUD de plantas del HOTEL)
		Administrar tipo de habitaciones
		Administrar Habitaciones (CRUD de habitaciones)
			Una habitacion pertenece a una planta
			Una habitacion tiene un tipo de habitacion
			Nombre de habitacion
			Precio de habitacion actual
			Si la habitacion esta o no libre

		Administrar clientes (CRUD de clientes)
			Alta de clientes

		Ocupar habitaciones
			Listado de habitaciones disponibles
			Elegir habitacion para ocupar
			Fecha de entrada y salida
			Calcular precio

		Desocupar habitacion

		Queremos acceder a un historial de las ocupaciones
		Queremos acceder a un historial de las ocupaciones realizadas por un cliente.