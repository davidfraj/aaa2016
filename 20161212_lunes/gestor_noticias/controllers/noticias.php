<?php  
//Archivo que llamara al modelo de datos, y cargara la vista
// controllers/noticias.php

include('models/NoticiasModel.php');
$modelo=new NoticiasModel();
$modelo->elementosPorPagina(10);

//Quiero recoger el numero de pagina por GET
//Sera la pagina actual 1, 2, 3, 4

$elementos=$modelo->listado($numeroDePagina); //Saco las noticias
$paginas=$modelo->paginas; //Saco cuantas paginas tengo

include('views/NoticiasView.php');

?>