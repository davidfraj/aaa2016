#include <stdio.h>
#include <windows.h>
#include <time.h>

int aleatorio(int min, int max){
	return rand()%(max-min+1)+min;
}

int main(){
		//Para el tamanyo del vector
		int T=10;
		//Para contar el numero de aleatorios
		int intentos=0;
		//Auxiliares para mover los numeros
		int posicion=0, aux=0;
		//Semilla de tiempo aleatoria
		srand(time(NULL));
		//Me creo un vector con 10 numeros
		int numeros[T];
		//Relleno los numeros desde el 1
		for(int i=0; i<T; i++){
			numeros[i]=(i+1);
		}
		//Saco los aleatorios
		for(int i=0; i<T; i++){
			posicion=aleatorio(0, T-1-i);
			aux=numeros[posicion];
			numeros[posicion]=numeros[T-1-i];
			numeros[T-1-i]=aux;
			intentos++;
		}
		//Muestro el vector
		for(int i=0; i<T; i++){
			printf("%d ",numeros[i]);
		}
		//Muestro el numero de intentos
		printf("\r\n\r\n%d intentos", intentos);
		getchar();
}
