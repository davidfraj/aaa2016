-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-11-2016 a las 20:19:59
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `empresa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
`idProducto` mediumint(8) unsigned NOT NULL,
  `nombreProducto` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `precioProducto` mediumint(9) DEFAULT NULL,
  `stockProducto` mediumint(9) DEFAULT NULL,
  `imagenProducto` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `mostrarProducto` tinyint(1) NOT NULL DEFAULT '1',
  `idCategoria` int(11) NOT NULL,
  `idMarca` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProducto`, `nombreProducto`, `precioProducto`, `stockProducto`, `imagenProducto`, `mostrarProducto`, `idCategoria`, `idMarca`) VALUES
(1, 'Metformin HCl', 13, 5, '', 1, 0, 0),
(2, 'Simvastatin', 78, 8, '', 1, 0, 0),
(3, 'Pravastatin Sodium', 87, 8, '', 1, 0, 0),
(4, 'Triamcinolone Acetonide', 97, 5, '', 1, 0, 0),
(7, 'Klor-Con M20', 29, 9, '', 1, 0, 0),
(8, 'Spiriva Handihaler', 93, 3, '', 1, 0, 0),
(9, 'Hydrochlorothiazide', 93, 5, '', 1, 0, 0),
(10, 'Hydrocodone/APAP', 46, 1, '', 1, 0, 0),
(11, 'Gabapentin', 46, 5, '', 1, 0, 0),
(12, 'Promethazine HCl', 35, 2, '', 1, 0, 0),
(13, 'Naproxen', 62, 5, '', 1, 0, 0),
(14, 'Alprazolam', 55, 7, '', 1, 0, 0),
(15, 'Amoxicillin', 10, 3, '', 1, 0, 0),
(16, 'Prednisone', 49, 2, '', 1, 0, 0),
(17, 'Nexium', 75, 6, '', 1, 0, 0),
(18, 'Sertraline HCl', 11, 9, '', 1, 0, 0),
(19, 'Furosemide', 27, 7, '', 1, 0, 0),
(20, 'Alprazolam', 15, 4, '', 1, 0, 0),
(21, 'Warfarin Sodium', 23, 1, '', 1, 0, 0),
(22, 'Lorazepam', 75, 3, '', 1, 0, 0),
(23, 'Vytorin', 75, 4, '', 1, 0, 0),
(24, 'Cialis', 43, 5, '', 1, 0, 0),
(25, 'Celebrex', 57, 2, '', 1, 0, 0),
(26, 'Lisinopril', 79, 5, '', 1, 0, 0),
(27, 'Amphetamine Salts', 51, 2, '', 1, 0, 0),
(28, 'Zolpidem Tartrate', 50, 8, '', 1, 0, 0),
(29, 'Lovaza', 64, 1, '', 1, 0, 0),
(30, 'TriNessa', 49, 1, '', 1, 0, 0),
(31, 'LevothyroxineSodium', 95, 5, '', 1, 0, 0),
(32, 'Lisinopril', 31, 1, '', 1, 0, 0),
(33, 'Klor-Con M20', 58, 9, '', 1, 0, 0),
(34, 'Hydrochlorothiazide', 37, 3, '', 1, 0, 0),
(35, 'Prednisone', 42, 6, '', 1, 0, 0),
(36, 'Tricor', 9, 1, '', 1, 0, 0),
(37, 'Fluticasone Propionate', 83, 3, '', 1, 0, 0),
(38, 'Doxycycline Hyclate', 26, 7, '', 1, 0, 0),
(40, 'Oxycodone/APAP', 82, 2, '', 1, 0, 0),
(41, 'Clindamycin HCl', 41, 4, '', 1, 0, 0),
(42, 'Cheratussin AC', 43, 2, '', 1, 0, 0),
(43, 'Pantoprazole Sodium', 90, 1, '', 1, 0, 0),
(44, 'Prednisone', 72, 4, '', 1, 0, 0),
(45, 'Warfarin Sodium', 39, 3, '', 1, 0, 0),
(46, 'Glipizide', 65, 3, '', 1, 0, 0),
(47, 'Sertraline HCl', 61, 2, '', 1, 0, 0),
(48, 'Benicar HCT', 78, 2, '', 1, 0, 0),
(49, 'Lorazepam', 45, 10, '', 1, 0, 0),
(50, 'Ventolin HFA', 8, 9, '', 1, 0, 0),
(51, 'Oxycontin', 41, 3, '', 1, 0, 0),
(52, 'Triamcinolone Acetonide', 57, 4, '', 1, 0, 0),
(53, 'Oxycodone/APAP', 57, 5, '', 1, 0, 0),
(54, 'Klor-Con M20', 81, 5, '', 1, 0, 0),
(55, 'Vitamin D (Rx)', 78, 6, '', 1, 0, 0),
(56, 'Simvastatin', 42, 2, '', 1, 0, 0),
(57, 'Ventolin HFA', 17, 3, '', 1, 0, 0),
(58, 'Namenda', 64, 9, '', 1, 0, 0),
(59, 'Tramadol HCl', 98, 9, '', 1, 0, 0),
(60, 'Cialis', 41, 2, '', 1, 0, 0),
(61, 'Ciprofloxacin HCl', 95, 1, '', 1, 0, 0),
(62, 'Atenolol', 15, 5, '', 1, 0, 0),
(63, 'Naproxen', 87, 1, '', 1, 0, 0),
(64, 'Carisoprodol', 21, 2, '', 1, 0, 0),
(65, 'Simvastatin', 36, 6, '', 1, 0, 0),
(66, 'Cephalexin', 55, 6, '', 1, 0, 0),
(67, 'Venlafaxine HCl ER', 62, 2, '', 1, 0, 0),
(68, 'Simvastatin', 65, 1, '', 1, 0, 0),
(69, 'Alendronate Sodium', 65, 10, '', 1, 0, 0),
(70, 'Meloxicam', 42, 4, '', 1, 0, 0),
(71, 'Clindamycin HCl', 29, 3, '', 1, 0, 0),
(72, 'Nexium', 98, 5, '', 1, 0, 0),
(73, 'Metformin HCl', 67, 9, '', 1, 0, 0),
(74, 'Lantus', 19, 3, '', 1, 0, 0),
(75, 'Clonazepam', 16, 10, '', 1, 0, 0),
(76, 'Amoxicillin Trihydrate/Potassium Clavulanate', 92, 8, '', 1, 0, 0),
(77, 'Gabapentin', 39, 10, '', 1, 0, 0),
(78, 'Zyprexa', 56, 5, '', 1, 0, 0),
(79, 'Hydrocodone/APAP', 21, 6, '', 1, 0, 0),
(80, 'Lipitor', 52, 5, '', 1, 0, 0),
(81, 'Loestrin 24 Fe', 35, 1, '', 1, 0, 0),
(82, 'Amlodipine Besylate', 28, 6, '', 1, 0, 0),
(83, 'Tri-Sprintec', 43, 10, '', 1, 0, 0),
(84, 'Cyclobenzaprin HCl', 28, 4, '', 1, 0, 0),
(85, 'Doxycycline Hyclate', 81, 10, '', 1, 0, 0),
(86, 'Prednisone', 53, 8, '', 1, 0, 0),
(87, 'Cyclobenzaprin HCl', 81, 1, '', 1, 0, 0),
(88, 'Amlodipine Besylate', 46, 6, '', 1, 0, 0),
(89, 'Nexium', 78, 4, '', 1, 0, 0),
(90, 'Crestor', 44, 3, '', 1, 0, 0),
(91, 'Ciprofloxacin HCl', 24, 2, '', 1, 0, 0),
(92, 'Losartan Potassium', 25, 5, '', 1, 0, 0),
(93, 'Warfarin Sodium', 13, 6, '', 1, 0, 0),
(94, 'Folic Acid', 17, 3, '', 1, 0, 0),
(95, 'Carvedilol', 5, 10, '', 1, 0, 0),
(96, 'Zolpidem Tartrate', 44, 9, '', 1, 0, 0),
(97, 'Paroxetine HCl', 31, 6, '', 1, 0, 0),
(98, 'Hydrocodone/APAP', 17, 8, '', 1, 0, 0),
(99, 'Meloxicam', 10, 3, '', 1, 0, 0),
(100, 'Carvedilol', 71, 1, '', 1, 0, 0),
(102, 'aaaa2', 122, 132, '1479325363_Penguins.jpg', 0, 1, 0),
(104, '111', 1111, 1111, '1479325377_Tulips.jpg', 0, 4, 1),
(105, '2222', 2222, 2222, '1479324737_Hydrangeas.jpg', 1, 2, 0),
(106, '1111111111', 1, 1, '', 1, 1, 1),
(107, '111', 111, 111, '', 1, 3, 0),
(108, '1111', 111, 1111, '', 1, 3, 0),
(109, '111', 111, 111, '', 1, 3, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
 ADD PRIMARY KEY (`idProducto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
MODIFY `idProducto` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=111;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
