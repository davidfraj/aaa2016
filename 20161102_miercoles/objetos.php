<?php 
include('includes/lista.class.php');
include('includes/calendario.class.php');
include('includes/desplegable.class.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Usando objetos en php</title>
</head>
<body>
	<?php 
	// $vector=array('Ocho', 'Nueve', 'Diez');
	// $lista=new Lista($vector);

	$lista=new Lista(array('Ocho', 'Nueve', 'Diez'));

	//Acceso a la propiedades
	//$lista->elementos=array('Uno', 'Dos', 'Tres');
	//Acceso a los metodos
	$lista->agregar(array('Uno', 'Dos', 'Tres'));
	$lista->agregar('Cuatro');
	$lista->agregar('Cinco');
	$lista->agregar('Seis');
	$lista->agregar('Siete');

	echo $lista->dibujar(); //Lista desordenada (ul)
	echo $lista->dibujar('ord'); //Lista ordenada (ol) 


	//Creamos una clase Calendario
	$cal=new Calendario(); //Sin nada, es año y mes actual
	//$cal=new Calendario($anyo, $mes);

	echo $cal->dibujaMes();
	echo $cal->dibujaAnyo();


	//Le pasamos, inicio, fin y actual
	$d= new Desplegable(1, 31, 5);
	echo $d->dibujar();

	?>
</body>
</html>
