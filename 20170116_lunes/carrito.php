<h3>Cesta de compra</h3>
<?php  
//Voy a tener una VARIABLE de session para la cesta
//Lo primero SIEMPRE es comprobar SI existe esa variable
if(!isset($_SESSION['cesta'])){
	$_SESSION['cesta']=array();
	$_SESSION['unidades']=array();
}

//Agregar productos
if(isset($_GET['idCompra'])){
	$buscar=array_search($_GET['idCompra'], $_SESSION['cesta']);
	if($buscar===false){
		$_SESSION['cesta'][]=$_GET['idCompra'];
		$_SESSION['unidades'][]=1;
	}else{
		$_SESSION['unidades'][$buscar]++;
	}
}

//Quitar productos
if(isset($_GET['quitar'])){
	array_splice($_SESSION['cesta'], $_GET['quitar'], 1);
	array_splice($_SESSION['unidades'], $_GET['quitar'], 1);
}

//aumentar productos
if(isset($_GET['aumentar'])){
	$_SESSION['unidades'][$_GET['aumentar']]++;
}

//disminuir productos
if(isset($_GET['disminuir'])){
	if($_SESSION['unidades'][$_GET['disminuir']]>1){
		$_SESSION['unidades'][$_GET['disminuir']]--;
	}else{
		array_splice($_SESSION['cesta'], $_GET['disminuir'], 1);
		array_splice($_SESSION['unidades'], $_GET['disminuir'], 1);
	}
}

//Vaciar el carrito
if(isset($_GET['vaciarcesta'])){
	$_SESSION['cesta']=array();
	$_SESSION['unidades']=array();
}

//Mostrar el carrito
$total=0;
$unidades=0;
for ($i=0; $i < count($_SESSION['cesta']); $i++) { 

	$sql="SELECT * FROM productos WHERE idPro=".$_SESSION['cesta'][$i];
	$consulta=$conexion->query($sql);
	$fila=$consulta->fetch_array();
	echo $fila['nombrePro'].'('.$fila['precioPro'].' €) ';
	echo $_SESSION['unidades'][$i].' unds';
	echo '<a href="index.php?quitar='.$i.'">Q</a>';
	echo '<a href="index.php?aumentar='.$i.'">+</a>';
	echo '<a href="index.php?disminuir='.$i.'">-</a>';
	echo '<br>';
	$total+=($fila['precioPro']*$_SESSION['unidades'][$i]);
	$unidades+=$_SESSION['unidades'][$i];

}

?>
<p><?php echo $unidades; ?> productos</p>
<p>Total: <?php echo $total; ?> Euros</p>
<a href="index.php?vaciarcesta=si">Vaciar</a>