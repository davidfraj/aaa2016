<?php 

//PASO 1:
//Quiero que una variable de session, controle
//si estoy o no conectado
//Me aseguro, que si NO existe la variable de 
//session, la declaro
if(!isset($_SESSION['conectado'])){
	
	$_SESSION['conectado']=null;

	if(isset($_COOKIES['mantener'])){
		$mantener=$_COOKIES['mantener'];
		$id=$_COOKIES['id'];
		$sql="SELECT * FROM usuarios WHERE correoUsuario='$mantener' AND fechaUsuario='$id'";
		$consulta=$conexion->query($sql);
		if($consulta->num_rows>0){
			$_SESSION['conectado']=new Usuario($consulta->fetch_array());
			//CREO LA COOKIE COMPLEJA
			$id=md5(time());
			setcookie('id',$id,time()+(24*60*60));
			$sql="UPDATE usuarios SET fechaUsuario='$id' WHERE correoUsuario='$mantener'";
			$conexion->query($sql);
			//FIN DE COOKIE

		}
	}
	
}


//PASO 2:
//Comprobamos si queremos conectarnos
if(isset($_POST['conectar'])){
	$usuario=$_POST['usuario'];
	$clave=md5($_POST['clave']);
	$sql="SELECT * FROM usuarios WHERE nombreUsuario='$usuario' AND claveUsuario='$clave'";
	$consulta=$conexion->query($sql);
	if($consulta->num_rows>0){
		//Exito al conectar
		$_SESSION['conectado']=new Usuario($consulta->fetch_array());

		//Compruebo si ha marcado NO CERRAR SESSION
		if(isset($_POST['mantener'])){
			setcookie('mantener', $_SESSION['conectado']->correo, time()+(24*60*60));
			//////CREO LA COOKIE COMPLEJA
			$id=md5(time());
			$mantener=$_SESSION['conectado']->correo;
			setcookie('id',$id,time()+(24*60*60));
			$sql="UPDATE usuarios SET fechaUsuario='$id' WHERE correoUsuario='$mantener'";
			$conexion->query($sql);
			//FIN DE COOKIE COMPLEJA
		}

	}else{
		//Error al conectar
	}
}

//PASO 3:
if(isset($_GET['desconectar'])){
	$_SESSION['conectado']=null;
	setcookie('mantener',null,time());
	setcookie('id',null,time());
}


//PASO 4:
//Distinguir si el usuario esta o no conectado
if($_SESSION['conectado']){
	//Si el usuario esta conectado
	echo 'Bienvenido '.$_SESSION['conectado']->correo;
	echo ' - <a href="index.php?desconectar=si">Desconectar</a>';
}else{
	//El usuario NO esta conectado
	?>
	<form action="index.php" method="post">
		<input type="text" placeholder="Usuario" name="usuario">
		<input type="password" placeholder="Clave" name="clave">
		No cerrar sesion<input type="checkbox" name="mantener">
		<input type="submit" name="conectar" value="Conectar">
	</form>
	<?php
}


?>