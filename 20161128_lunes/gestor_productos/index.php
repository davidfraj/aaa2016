<?php 
include('includes/conexion.php');
include('includes/funciones.php');


// $sql="SELECT * FROM productos";
// $consulta=$conexion->query($sql); //Devuelve un objeto de la class mysqli_result

// foreach ($consulta->fetch_fields() as $key => $value) {
// 	echo $value->name;
// 	echo '<br>';
// }


if(isset($_GET['p'])){
	$p=$_GET['p'];
}else{
	$p='productos.php';
}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Gestor de productos</title>
	<link rel="stylesheet" href="css/estilos.css">
</head>
<body>
	<!-- section#contenedor>(header#encabezado+nav#menu+section#principal+footer#pie) -->
	<section id="contenedor">
		<header id="encabezado">Encabezado</header>
		<nav id="menu">Menu</nav>
		<section id="principal">
			<?php include('paginas/'.$p); ?>
		</section>
		<footer id="pie">Pie</footer>
	</section>
</body>
</html>
<?php 
$conexion->close();
?>