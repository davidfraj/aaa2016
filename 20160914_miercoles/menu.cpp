//PROGRAMA menu
//FECHA 14 de Septiembre de 2016
#include <stdio.h>
#include <windows.h>
int main(){
		//Variable para recoger la opcion del usuarios
		int opcion=0;
		int vector[5]={8, 12, 96, 42, 28};
		int apuesta[5]={0, 0, 0, 0, 0};
		int par, impar, suma, mayor, menor, aux, encontrada, aciertos;

		do{
			system("cls");
			printf("PROGRAMA CON MENU\r\n");
			printf("-----------------\r\n");
			printf("1.- Muestra par e impar\r\n");
			printf("2.- Muestra suma de vector\r\n");
			printf("3.- Muestrame el mayor numero\r\n");
			printf("4.- Muestrame el menor numero\r\n");
			printf("5.- Rellenar Vector\r\n");
			printf("6.- Mostrar Vector\r\n");
			printf("7.- Juego de bonoloto\r\n");
			printf("0.- Salir de la aplicacion\r\n");
			printf("Elige una opcion: ");
			scanf("%d", &opcion);
			fflush(stdin);
			switch(opcion){
				case 1:
					par=0;
					impar=0;
					printf("Mostramos par e impar");
					for(int i=0; i<5; i++){
						if(vector[i]%2==0){
							par++;
						}else{
							impar++;
						}
					}
					printf("El numero pares es %d", par);
					printf("El numero impares es %d", impar);

					getchar();
					break;
				case 2:
					printf("Mostramos la suma");
					suma=0;

					for(int i=0; i<5; i++){
						suma+=vector[i];
					}
					printf("La suma es %d", suma);

					getchar();
					break;
				case 3:
					printf("Mostramos mayor");
					getchar();
					mayor=vector[0];
					for(i=1; i<5;i++){
						if(vector[i]>mayor){
							mayor=vector[i];
						}
					}
					printf("El mayor es %d", mayor);
					break;
				case 4:
					printf("Mostramos menor");
					menor=vector[0];
					for(i=1; i<5;i++){
						if(vector[i]<menor){
							menor=vector[i];
						}
					}
					printf("El menor es %d", menor);
					getchar();
					break;

				case 5:
					printf("Cambiamos el vector");
					
					for(i=0; i<5;i++){
						printf("Escribe el numero %d:", (i+1));
						scanf("%d", &vector[i]);
						fflush(stdin);
					}
					
					getchar();
					break;
				case 6:
					printf("Mostramos el vector");
					
					for(i=0; i<5;i++){
						printf("%d ", vector[i]);
					}
					
					getchar();
					break;

				case 7:
					printf("Introduce 5 numeros: ");
					//Rellenamos el vector con numeros DIFERENTES
					for(i=0; i<5;i++){
						do{
							printf("Escribe el numero %d:", (i+1));
							scanf("%d", &aux);
							fflush(stdin);
							//Busco el numero en mi vector de apuestas
							encontrada=0;
							for(int j=0; j<i; j++){
								if(apuesta[j]==aux){
									encontrada=1;
								}
							}

						}while(encontrada==1);
						apuesta[i]=aux;
						
					}
					
					//Vamos a comprobar, que numeros del
					// vector apuesta, aparecen en 
					//el vector "vector"
					aciertos=0;
					for(int i=0; i<5; i++){
						for(int j=0; j<5; j++){
							if(vector[i]==apuesta[j]){
								aciertos++;
							}
						}
					}
					printf("El numero de aciertos es %d.", aciertos);




					getchar();
					break;


			}

		}while(opcion!=0);

}
