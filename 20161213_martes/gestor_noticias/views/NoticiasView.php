<?php  
// views/NoticiasView.php
//Parto de que mi coleccion de datos, esta en 
//el vector $elementos
?>
<?php foreach ($elementos as $e){ ?>
<article>
	<header>
		<?php echo $e->dimeTitulo();?>
		 - 
		<?php echo $e->dimeFecha();?>
	</header>
	<section>
		<?php echo $e->dimeContenido();?>
	</section>
	<footer>
		<?php echo $e->dimeAutor();?>
	</footer>
</article>
<?php } ?>

<hr>

<?php for ($i=1; $i <= $paginas; $i++) { ?>
	<a href="index.php?p=noticias.php&numpag=<?php echo $i;?>">
		<?php echo $i;?>
	</a>
<?php } ?>
