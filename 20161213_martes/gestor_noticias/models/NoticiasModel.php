<?php  
//Incluyo mi modelo de noticia individual
include('models/NoticiaModel.php');
//Modelo de noticias
Class NoticiasModel{
	private $conexion;
	private $elementos;
	private $elementosPorPagina;

	public function __construct(){
		$this->conexion=Conexion::conectar();
		$this->elementos=array();
		$this->elementosPorPagina=5;
	}
	public function listado($numpag){
		$n=($numpag-1)*$this->elementosPorPagina;
		$sql="SELECT * FROM noticias LIMIT $n,$this->elementosPorPagina";
		$consulta=$this->conexion->query($sql);
		while($fila=$consulta->fetch_array()){
			$not=new NoticiaModel($fila['idNot'], $fila['tituloNot'], $fila['contenidoNot'], $fila['autorNot'], $fila['fechaNot']);
			$this->elementos[]=$not;
		}
		return $this->elementos;
	}
	public function elementosPorPagina($numElementos){
		if($numElementos>0){
			$this->elementosPorPagina=$numElementos;
		}
	}
	public function dimePaginas(){
		$sql="SELECT * FROM noticias";
		$consulta=$this->conexion->query($sql);
		$total=$consulta->num_rows;
		//los dividimos para el numero de elementos por pagina
		//Lo redondeamos ceil(numero)
		//Lo devolvemos
		return ceil($total/$this->elementosPorPagina);
	}
}
?>