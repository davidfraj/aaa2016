<?php 
if(isset($_POST["subir"])){
	//Si venimos de pulsar el boton subir
	if(is_uploaded_file($_FILES["imagen"]["tmp_name"])){
		echo "<br>".$_FILES["imagen"]["name"];
		echo "<br>".$_FILES["imagen"]["size"];
		echo "<br>".$_FILES["imagen"]["type"];
		echo "<br>".$_FILES["imagen"]["tmp_name"];

		$nombre=time().'_'.$_FILES["imagen"]["name"];
		
		move_uploaded_file($_FILES["imagen"]["tmp_name"], "img/".$nombre);
	}
}

//Voy a preguntar si quiero Borrar la imagen
if(isset($_GET["borrar"])){
	$borrar=$_GET["borrar"];
	$ruta="img/$borrar";
	if(is_file($ruta)){
		if(!unlink($ruta)){
			echo "No se ha podido borrar";
		}
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>directorios.php</title>
</head>
<body>
	<h1>Queremos abrir el directorio IMG</h1>
	<hr>
	<form action="directorios.php" method="post" enctype="multipart/form-data">
		<input type="file" name="imagen">
		<input type="submit" name="subir" value="subir">
	</form>
	<hr>
	<?php 
	//Variable para la primera imagen
	$primera=NULL;
	//Establecemos el directorio:
	$directorio="img";
	//Abrimos el directorio
	if($dir=opendir($directorio)){
		//Leemos el directorio
		while($elemento=readdir($dir)){
			if(is_file("$directorio/$elemento")){
				if($primera==NULL){
					$primera=$elemento;
				}
				echo '<a href="directorios.php?grande='.$elemento.'">';
				echo '<img src="'.$directorio.'/'.$elemento.'" width="100">';
				echo '</a>';
				echo '<a href="directorios.php?borrar='.$elemento.'">';
				echo '<img src="borrar.png" width="20">';
				echo '</a>';
			}
		}

		//Si queremos REBOBINAR el directorio:
		rewinddir($dir);

		//Cerramos el directorio
		closedir($dir);
		//Mostramos la imagen en grande
		echo '<hr>';
		if(isset($_GET['grande'])){
			$grande=$_GET['grande'];
		}else{
			$grande=$primera;
		}
		echo '<img src="'.$directorio.'/'.$grande.'" width="400">';
	}else{
		echo "Error al abrir el directorio";
	}
	?>
</body>
</html>