//PROGRAMA bucles04
//Pedimos al usuario un numero por pantalla
// del 1 al 50. 
// Dibujamos todos los numeros del 1 al 50, de 
//5 en 5, y el numero introducido por teclado
//Lo sustituimos por **

 //1  2  3  4  5
 //6  7  8  9 10
 //11 12 ** 14 15



//FECHA 12 de Septiembre de 2016
#include <stdio.h>
int main(){
		int i;
		int numero;

		printf("Escribe el numero:");
		scanf("%d", &numero);
		fflush(stdin);

		//Cuando sabemos inicio y fin
		for(i=1; i<=50; i++){
			if(i==numero){
				printf("** ");
			}else{
				printf("%2d ", i);
			}
			if(i%5==0){
				printf("\r\n");
			}
		}

		//Pedimos un intro para Salir
		getchar();
}