//PROGRAMA bucles03
//Usando un bucle while, queremos que el programa nos pida
//numeros por pantalla, mientras que el numero sea superior a 0
//En cuanto pongamos un 0, deja de pedir numeros, y nos muestra 
//la suma de todos los numeros que hemos introducido

//FECHA 12 de Septiembre de 2016
#include <stdio.h>
int main(){
		int i;
		int c=9;

		//Cuando sabemos inicio y fin
		for(i=1; i<=500; i++){
			if(i<10){
				printf(" ");
			}
			if(i<100){
				printf(" ");
			}
			printf("%d ", i);
			if(i%c==0){
				printf("\r\n");
			}
		}
		
		//operacion modulo (resto de division)
		//Queremos mostrar, los numeros del 1 al 80
		//Dispuestos de 5 en 5
		// 1  2  3  4  5
		// 6  7  8  9 10
		//11 12 13 14 15



		//Pedimos un intro para Salir
		getchar();
}
