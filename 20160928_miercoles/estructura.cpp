#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

struct fecha{
	int dia;
	int mes;
	int anyo;
};

struct producto{
	char nombre[50];
	float precio;
	int unidades;
	struct fecha fcompra;
};

int main(){
	
	struct producto pro;
	
	strcpy(pro.nombre, "Impresora");
	pro.precio=50.6;
	pro.unidades=5;
	pro.fcompra.dia=20;
	pro.fcompra.mes=11;
	pro.fcompra.anyo=2015;
	
	struct producto almacen[5];
	almacen[0].unidades=6;
	almacen[0].fcompra.dia=5;
	
	struct fecha mifecha;
	mifecha.dia=28;
	mifecha.mes=9;
	mifecha.anyo=2016;

	struct fecha vacaciones[3];

	vacaciones[0].dia=28;
	vacaciones[0].mes=9;
	vacaciones[0].anyo=2016;

	vacaciones[1].dia=6;
	vacaciones[1].mes=1;
	vacaciones[1].anyo=2014;

	vacaciones[2].dia=8;
	vacaciones[2].mes=12;
	vacaciones[2].anyo=2016;

	//Averiguo en tama�o de mi vector
	int t1=sizeof(vacaciones);
	printf("\r\n %d \r\n", t1);
	
   int t2=sizeof(vacaciones[0]);
	printf("\r\n %d \r\n", t2);
	
	//Este sera el numero de elementos del vector
	int t=t1/t2;

	for(int i=0; i<sizeof(vacaciones)/sizeof(vacaciones[0]); i++){
		printf("%d/%d/%d\r\n",vacaciones[i].dia, vacaciones[i].mes, vacaciones[i].anyo);
	}

		     

	char dias[][50]={"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};

	printf("%s", dias[0]);
	printf("%c", dias[1][0]); //m

	//pido getchar();
	getchar();
}
