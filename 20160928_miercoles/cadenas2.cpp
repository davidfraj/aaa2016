#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int contar(char *nombre){
	int contador=0;
	while(nombre[contador]!='\0'){
		contador++;
	}
	return contador;
}

int contar(char *nombre, char letra){
	int contador=0;
	int cantidad=0;
	while(nombre[contador]!='\0'){
		if(letra==nombre[contador]){
         cantidad++;
		}
		contador++;
	}
	return cantidad;
}

int main(){
	
	char nombre[]="lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat. duis aute irure dolor in reprehenderit in voluptate velit essecillum dolore eu fugiat nulla pariatur";
	printf("%d caracteres\r\n", contar(nombre, 'a'));
	
	char abc[]="abcdefghijklmnopqrstuvwxyz";
	
	for(int i=0; abc[i]!='\0';i++){
      printf("Letra %c: %d veces\r\n",abc[i], contar(nombre, abc[i]));
	}
	
	//pido getchar();
	getchar();
}
