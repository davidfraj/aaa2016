<?php 

class Desplegable{
	//Propiedades
	private $inicio;
	private $final;
	private $actual;

	//Metodos
	public function __construct($inicio, $final, $actual){
		$this->inicio=$inicio;
		$this->final=$final;
		$this->actual=$actual;
	}

	public function dibujar(){
		$r='';
		$r.='<select>';
		for ($i=$this->inicio; $i <= $this->final; $i++) { 
			
			if($i==$this->actual){
				$sel='selected';
			}else{
				$sel='';
			}
			$r.='<option value="'.$i.'" '.$sel.'>'.$i.'</option>';

		}
		$r.='</select>';
		return $r;
	}
}



?>