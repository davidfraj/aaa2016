<?php 
class Area{
	//
	public $tipo;
	public $coordenadas;
	public $enlace;

	//Metodos
	public function __construct($tipo, $coordenadas, $enlace){
		$this->tipo=$tipo;
		$this->coordenadas=$coordenadas;
		$this->enlace=$enlace;
	}

	public function dibujar(){
		return '<area shape="'.$this->tipo.'" coords="'.$this->coordenadas.'" href="'.$this->enlace.'" >';
	}

}
?>