<?php 

//Clase para crear mapas de imagenes en HTML

class Mapa extends Imagen{

	//Creo propiedad del Mapa
	public $nombre;
	public $areas; //Vector con las areas del mapa

	//Metodos de mi clase
	public function __construct($archivo, $titulo, $nombre){
		parent::__construct($archivo, $titulo);
		$this->nombre=$nombre;
		$this->areas=array();
	}

	public function dibujar(){
		$r=substr(parent::dibujar(), 0, strlen(parent::dibujar())-5);
		$r.=' usemap="#'.$this->nombre.'"><br>';
		$r.='<map name="'.$this->nombre.'">';
		foreach ($this->areas as $area) {
			$r.=$area->dibujar();
		}
		$r.='</map>';
		return $r;
	}

	public function getAncho(){
		return $this->ancho;
	}

	public function addArea($tipo, $coordenadas, $enlace){
		$area=new Area($tipo, $coordenadas, $enlace);
		$this->areas[]=$area;
	}

}


?>