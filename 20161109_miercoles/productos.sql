DROP TABLE `productos`;

CREATE TABLE `productos` (
  `idProducto` mediumint(8) unsigned NOT NULL auto_increment,
  `nombreProducto` varchar(255) default NULL,
  `precioProducto` mediumint default NULL,
  `stockProducto` mediumint default NULL,
  PRIMARY KEY (`idProducto`)
) AUTO_INCREMENT=1;

INSERT INTO `productos` (`idProducto`,`nombreProducto`,`precioProducto`,`stockProducto`) VALUES (1,"Metformin HCl",13,5),(2,"Simvastatin",78,8),(3,"Pravastatin Sodium",87,8),(4,"Triamcinolone Acetonide",97,5),(5,"Amoxicillin",26,4),(6,"Amoxicillin Trihydrate/Potassium Clavulanate",73,2),(7,"Klor-Con M20",29,9),(8,"Spiriva Handihaler",93,3),(9,"Hydrochlorothiazide",93,5),(10,"Hydrocodone/APAP",46,1);
INSERT INTO `productos` (`idProducto`,`nombreProducto`,`precioProducto`,`stockProducto`) VALUES (11,"Gabapentin",46,5),(12,"Promethazine HCl",35,2),(13,"Naproxen",62,5),(14,"Alprazolam",55,7),(15,"Amoxicillin",10,3),(16,"Prednisone",49,2),(17,"Nexium",75,6),(18,"Sertraline HCl",11,9),(19,"Furosemide",27,7),(20,"Alprazolam",15,4);
INSERT INTO `productos` (`idProducto`,`nombreProducto`,`precioProducto`,`stockProducto`) VALUES (21,"Warfarin Sodium",23,1),(22,"Lorazepam",75,3),(23,"Vytorin",75,4),(24,"Cialis",43,5),(25,"Celebrex",57,2),(26,"Lisinopril",79,5),(27,"Amphetamine Salts",51,2),(28,"Zolpidem Tartrate",50,8),(29,"Lovaza",64,1),(30,"TriNessa",49,1);
INSERT INTO `productos` (`idProducto`,`nombreProducto`,`precioProducto`,`stockProducto`) VALUES (31,"LevothyroxineSodium",95,5),(32,"Lisinopril",31,1),(33,"Klor-Con M20",58,9),(34,"Hydrochlorothiazide",37,3),(35,"Prednisone",42,6),(36,"Tricor",9,1),(37,"Fluticasone Propionate",83,3),(38,"Doxycycline Hyclate",26,7),(39,"Actos",100,5),(40,"Oxycodone/APAP",82,2);
INSERT INTO `productos` (`idProducto`,`nombreProducto`,`precioProducto`,`stockProducto`) VALUES (41,"Clindamycin HCl",41,4),(42,"Cheratussin AC",43,2),(43,"Pantoprazole Sodium",90,1),(44,"Prednisone",72,4),(45,"Warfarin Sodium",39,3),(46,"Glipizide",65,3),(47,"Sertraline HCl",61,2),(48,"Benicar HCT",78,2),(49,"Lorazepam",45,10),(50,"Ventolin HFA",8,9);
INSERT INTO `productos` (`idProducto`,`nombreProducto`,`precioProducto`,`stockProducto`) VALUES (51,"Oxycontin",41,3),(52,"Triamcinolone Acetonide",57,4),(53,"Oxycodone/APAP",57,5),(54,"Klor-Con M20",81,5),(55,"Vitamin D (Rx)",78,6),(56,"Simvastatin",42,2),(57,"Ventolin HFA",17,3),(58,"Namenda",64,9),(59,"Tramadol HCl",98,9),(60,"Cialis",41,2);
INSERT INTO `productos` (`idProducto`,`nombreProducto`,`precioProducto`,`stockProducto`) VALUES (61,"Ciprofloxacin HCl",95,1),(62,"Atenolol",15,5),(63,"Naproxen",87,1),(64,"Carisoprodol",21,2),(65,"Simvastatin",36,6),(66,"Cephalexin",55,6),(67,"Venlafaxine HCl ER",62,2),(68,"Simvastatin",65,1),(69,"Alendronate Sodium",65,10),(70,"Meloxicam",42,4);
INSERT INTO `productos` (`idProducto`,`nombreProducto`,`precioProducto`,`stockProducto`) VALUES (71,"Clindamycin HCl",29,3),(72,"Nexium",98,5),(73,"Metformin HCl",67,9),(74,"Lantus",19,3),(75,"Clonazepam",16,10),(76,"Amoxicillin Trihydrate/Potassium Clavulanate",92,8),(77,"Gabapentin",39,10),(78,"Zyprexa",56,5),(79,"Hydrocodone/APAP",21,6),(80,"Lipitor",52,5);
INSERT INTO `productos` (`idProducto`,`nombreProducto`,`precioProducto`,`stockProducto`) VALUES (81,"Loestrin 24 Fe",35,1),(82,"Amlodipine Besylate",28,6),(83,"Tri-Sprintec",43,10),(84,"Cyclobenzaprin HCl",28,4),(85,"Doxycycline Hyclate",81,10),(86,"Prednisone",53,8),(87,"Cyclobenzaprin HCl",81,1),(88,"Amlodipine Besylate",46,6),(89,"Nexium",78,4),(90,"Crestor",44,3);
INSERT INTO `productos` (`idProducto`,`nombreProducto`,`precioProducto`,`stockProducto`) VALUES (91,"Ciprofloxacin HCl",24,2),(92,"Losartan Potassium",25,5),(93,"Warfarin Sodium",13,6),(94,"Folic Acid",17,3),(95,"Carvedilol",5,10),(96,"Zolpidem Tartrate",44,9),(97,"Paroxetine HCl",31,6),(98,"Hydrocodone/APAP",17,8),(99,"Meloxicam",10,3),(100,"Carvedilol",71,1);
