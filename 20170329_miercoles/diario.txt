Miercoles 29 de MArzo de 2017
--------------------

Web services con PHP
--------------------

SOAP o REST

Necesito un servidor

	servidor.php
	cliente.php

Creamos un nuevo proyecto en git
	wssoap

creamos un directorio con ese nombre
	c:\xampp\htdocs\wssoap
	Copiamos el contenido del directorio lib de nuSoap

Me meto en linea de comandos, en el directorio
	git init
	git remote add origin https://davidfraj@bitbucket.org/davidfraj/wssoap.git
	git add .
	git commit -m "Mi primer commit"
	git push origin masters

Creamos el servidor.php

<?php  
//Archivo para crear un WEBSERVICE sencillo en php
//Usamos la libreria nuSOap

include('lib/nusoap.php');
$server = new soap_server();
$server->configureWSDL("servicionoticias", "urn:servicionoticias");

$conexion=new mysqli('localhost', 'root', '', 'empresa');
$conexion->set_charset('utf-8');

$server->register("getNoticia",
    array("idNoticia" => "xsd:integer"),
    array("return" => "xsd:string"),
    "urn:servicionoticias",
    "urn:servicionoticias#getNoticia",
    "rpc",
    "encoded",
    "Obtener noticia a traves del id");

$server->register("getContenido",
    array("idNoticia" => "xsd:integer"),
    array("return" => "xsd:string"),
    "urn:servicionoticias",
    "urn:servicionoticias#getContenido",
    "rpc",
    "encoded",
    "Obtener contenido de la noticia a traves del id");

function getNoticia($idNoticia){
	$sql="SELECT * FROM noticias WHERE idNot=$idNoticia";
	$consulta=$conexion->query($sql);
	$r=$consulta->fetch_array();
	return $r['tituloNot'];
}

function getContenido($idNoticia){
	$sql="SELECT * FROM noticias WHERE idNot=$idNoticia";
	$consulta=$conexion->query($sql);
	$r=$consulta->fetch_array();
	return $r['contenidoNot'];
}

//Devolvemos los datos que nos pidan
$server->service(file_get_contents("php://input"));

$conexion->close();
?>

mod_rewrite
-----------
.htaccess
rewriteRule /alcorcon/pisos/	index.php?p=$1 .php&hotel=$2

----------------------------

clientDistancia.php

En esta web:
http://www.webservicex.net/New/Home/ServiceDetail/10

Hay un WebService SOAP, que responde a esta direccion

http://www.webservicex.net/CurrencyConvertor.asmx?WSDL

Tiene una funcion, llamada "ConversionRate"

FromCurrency
ToCurrency