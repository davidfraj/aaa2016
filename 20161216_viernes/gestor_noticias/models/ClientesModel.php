<?php  
//Incluyo mi modelo de noticia individual
include('models/ClienteModel.php');
//Modelo de noticias
Class ClientesModel extends MasterModel{

	public function __construct(){
		parent::__construct('clientes');
		//$this->campoOrden='idCli'; //OPCIONAL
	}
	public function listado($numpag){
		$n=($numpag-1)*$this->elementosPorPagina;
		$sql="SELECT * FROM $this->tabla ORDER BY $this->campoOrden $this->orden LIMIT $n,$this->elementosPorPagina";
		$consulta=$this->conexion->query($sql);
		while($fila=$consulta->fetch_array()){
			$cli=new ClienteModel($fila['idCli'], $fila['nombreCli'], $fila['correoCli'], $fila['fechaAltaCli']);
			$this->elementos[]=$cli;
		}
		return $this->elementos;
	}
}
?>