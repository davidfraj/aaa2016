<?php  
//NoticiaModel.php
Class ClienteModel{
	private $idCli;
	private $nombreCli;
	private $correoCli;
	private $fechaAltaCli;

	public function __construct($idCli, $nombreCli, $correoCli, $fechaAltaCli){
		$this->idCli=$idCli;
		$this->nombreCli=$nombreCli;
		$this->correoCli=$correoCli;
		$this->fechaAltaCli=$fechaAltaCli;
	}
	public function dimeId(){
		return $this->idCli;
	}
	public function dimeNombre(){
		return $this->nombreCli;
	}
	public function dimeCorreo(){
		return $this->correoCli;
	}
	public function dimeFechaAlta(){
		return $this->fechaAltaCli;
	}
}


?>