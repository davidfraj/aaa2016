<?php  
//Archivo que llamara al modelo de datos, y cargara la vista
// controllers/noticias.php

include('models/Noticias2Model.php');
$modelo=new Noticias2Model();
$modelo->elementosPorPagina(10); //opcional
$modelo->estableceOrden('tituloNot', 'DESC');

//Quiero recoger el numero de pagina por GET
//Sera la pagina actual 1, 2, 3, 4
if(isset($_GET['numpag'])){
	$numpag=$_GET['numpag'];
}else{
	$numpag=1;
}

$elementos=$modelo->listado($numpag); //Saco las noticias
$paginas=$modelo->dimePaginas(); //Saco cuantas paginas tengo

include('views/Noticias2View.php');

?>