//PROGRAMA bucles1
//FECHA 09 de Septiembre de 2016
#include <stdio.h>

int main(){
	//VARIABLES
	int i,n,r;

	//INSTRUCCIONES
		i=1;
		n=2;
		int ultimo=10;
		//<, <=, >, >=, ==, !=
		while(i<=10){
			r=n*i;
			printf("%d por %d son %d\r\n",i,n,r);
			i++;
		}
		//usando un bucle While,
		//Sacar por pantalla la tabla
		// de multiplicar del 2
		
		//2 por 1 son 2
		//2 por 2 son 4
		//....
		//2 por 10 son 20

		int contador=10;
		for(i=0;i<=contador;i++){
			printf(" %d ",i);
			if(i<contador){
				printf("/");
			}
		}


		//Pedimos un intro para Salir
		getchar();
}
